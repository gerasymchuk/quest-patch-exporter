﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace amazon_xml_exporter
{
    class ArgumentsParser
    {
        private ArrayList doubleOptsList;
        private Dictionary<String, String> optsList;
            
        public void Parse(String[] args)
        {
            doubleOptsList = new ArrayList(); 
            optsList = new Dictionary<String, String>();
            try
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i][0].Equals('-'))
                    {
                        if (args[i].Length < 2)
                            throw new ArgumentException("Not a valid argument: " + args[i]);
                        if (args[i][1].Equals('-'))
                        {
                            if (args[i].Length < 3)
                                throw new ArgumentException("Not a valid argument: " + args[i]);
                            // --opt
                            doubleOptsList.Add(args[i].Substring(2));
                        }
                        else
                        {
                            if (args.Length - 1 == i)
                                throw new ArgumentException("Expected arg after: " + args[i]);
                            // -opt
                            optsList[args[i].Substring(1)] = args[i + 1];
                            i++;
                        }
                    }
                }
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public int GetArgAsInt(string name, int defaultValue)
        {
            int value;
            if (name.Length > 2 && optsList != null && optsList.ContainsKey(name.Substring(1)) && int.TryParse(optsList[name.Substring(1)], out value))
            {
                return value;
            }
            return defaultValue;
        }

        internal string GetArgAsString(string name, string defaultValue)
        {
            String value = GetArgAsString(name); 
            return value != null ? value : defaultValue;
        }

        public Boolean Contains(String name)
        {
            return (name.StartsWith("--") && doubleOptsList != null && doubleOptsList.Contains(name.Substring(2))) ||
                (name.StartsWith("-") && optsList != null && optsList.ContainsKey(name.Substring(1)));
        }

        public String GetArgAsString(String name)
        {
            if (name.Length > 2 && optsList != null && optsList.ContainsKey(name.Substring(1)))
            {
                return optsList[name.Substring(1)];
            }
            return null;
        }

    }

   
}
