﻿using System;
using System.Collections;
using System.Text;

namespace amazon_xml_exporter
{
    class PatchVO
    {
        public PatchVO()
        {
            Name = "";
            Ids = new ArrayList();
            Description = "";
            PatchType = "";
            Link = "";
        }

        public PatchVO(String name) : this()
        {
            Name = name;
        }

        public ArrayList Ids { get; set;}
        public string Name { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string PatchType { get; set; }
        public string ProductVersion { get; internal set; }

        public string ToCsv()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("\"{0}\",\"{1}\",\"{2}\",\"{3}\",,\"{4}\",\"{5}\"{6}", Name, ProductVersion, PatchType, Ids.Count > 0 ? Ids[0] : "", Description, Link, Environment.NewLine);
            if (Ids.Count > 1)
            {
                for (int i = 1; i < Ids.Count; i++)
                {
                    sb.AppendFormat("\"{0}\",\"{1}\",,\"{2}\"{3}", Name, ProductVersion, Ids[i], Environment.NewLine);
                }
            }
            return sb.ToString();
        }

    }
}
