﻿using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;

namespace amazon_xml_exporter
{
    class Program
    {
        private const string URL_PATTERN = "https://s3.amazonaws.com/appassure_patches/{0}-{1}/{0}-{1}.xml";
        private static readonly string[] TYPES = {"P", "P-A64", "P-A86", "P-M64", "P-L64", "P-L86", "P-FTBU", "P-SRM"};
        private static readonly Regex EXT_REGEX = new Regex(".xml$");
        private const string LOG_FILENAME = "log.txt";
        private const string DEFAULT_FILENAME = "export-{0:MM-dd-yy-H-mm-ss}.csv";
        
        private const int DEFAULT_START_INDEX = 1975;
        private const int DEFAULT_END_INDEX = 2052;

        static void Main(string[] args)
        {
            ArgumentsParser parser = new ArgumentsParser();
            parser.Parse(args);
            if (parser.Contains("--h"))
            {
                Console.WriteLine("-o <path>               : path to output file (optional), default is " + DEFAULT_FILENAME);
                Console.WriteLine("-start <id>             : start index (optional), default is " + DEFAULT_START_INDEX);
                Console.WriteLine("-end <id>               : end index (optional), default is " + DEFAULT_END_INDEX);
                Console.WriteLine("--h                     : help");
                return;
            }
            String outputPath = parser.GetArgAsString("-o", String.Format(DEFAULT_FILENAME, DateTime.Now));
            if (parser.Contains("--clear"))
            {
                File.Delete(outputPath);
            }

            int start = parser.GetArgAsInt("-start", DEFAULT_START_INDEX);
            int end = parser.GetArgAsInt("-end", DEFAULT_END_INDEX);
            if (start >= end) throw new ArgumentException("End should be greater than Start");

            Log("Output path: " + outputPath);
            Log(String.Format("From {0} to {1}", start, end));
            Log(String.Format("Types: {0}", string.Join(", ", TYPES)));
            Log("Start processing...");

            File.AppendAllText(outputPath, "CB,ProductVersion,Type,Defect ID,not documented id,Comment,Link" + Environment.NewLine);
           
            string url;
            for (int i = start; i <= end; i++)
            {
                foreach (String type in TYPES)
                {
                    url = String.Format(URL_PATTERN, type, i);
                    String data = GetData(url);
                    if (data != null)
                    {
                        Log(url, false);
                        Console.Write("\r{0}       ", url);

                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(data);
                        XmlNode root = doc.DocumentElement;
                        XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                        nsmgr.AddNamespace("i", "http://www.w3.org/2001/XMLSchema-instance");
                        nsmgr.AddNamespace("c", "http://schemas.datacontract.org/2004/07/PatchInstaller.Common.Contracts");
                        nsmgr.AddNamespace("d2p1", "http://apprecovery.com/management/api/2010/05");
                        nsmgr.AddNamespace("d3p1", "http://schemas.microsoft.com/2003/10/Serialization/Arrays");

                        PatchVO patch = new PatchVO(type + "-" + i);

                        foreach (XmlNode id in root.SelectNodes("c:patchInfo/d2p1:defectIds/d3p1:string", nsmgr))
                        {
                            patch.Ids.Add(id.InnerText);
                        }

                        XmlNode node = root.SelectSingleNode("c:patchInfo/d2p1:description", nsmgr);
                        if (node != null)
                        {
                            patch.Description = node.InnerText;
                        }
                        node = root.SelectSingleNode("c:patchInfo/d2p1:name", nsmgr);
                        if (node != null)
                        {
                            patch.Name = node.InnerText;
                        }
                        node = root.SelectSingleNode("c:patchInfo/d2p1:patchType", nsmgr);
                        if (node != null)
                        {
                            patch.PatchType = node.InnerText;
                        }
                        node = root.SelectSingleNode("c:patchInfo/d2p1:productVersion", nsmgr);
                        if (node != null)
                        {
                            patch.ProductVersion = node.InnerText;
                        }
                        

                        patch.Link = EXT_REGEX.Replace(url, ".msi");

                        File.AppendAllText(outputPath, patch.ToCsv());
                    }

                }
            }

        }

        private static void Log(string value, bool verbose = true)
        {
            File.AppendAllText(LOG_FILENAME, value);
            if(verbose)
            {
                Console.WriteLine(value);
            }
        }

        private static String GetData(String url) 
        {
            WebRequest request = WebRequest.Create(url) as HttpWebRequest;
            String result = null;
            try
            {
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    result = reader.ReadToEnd();
                    reader.Close();
                } 
                response.Close();
            }
            catch
            {
                //silence
            }
            return result;
        }
    }
}
